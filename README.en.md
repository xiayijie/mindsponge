# MindSPONGE

## Description

MindSPONGE (MindSpore Simulation Package tOwards Next Generation molecular modelling)

## Software Architecture

<div align=left>
<img src="docs/mindsponge.png" alt="MindSPONGE Architecture" width="600"/>
</div>

## Installation

This code was developed base on the AI framework MindSpore: https://mindspore.cn/

## Instructions

The files in the "tutorials" directory are the basic tutorials：

tutorial_b01.py: Create a simple simulation system manually.

tutorial_b02.py: Create a simple simulation system using template and parameters file.

tutorial_b03.py: Edit system and minimization.

tutorial_b04.py: MD simulation with bias potential.

tutorial_b05.py: MD simulation with periodic boundary condition.

tutorial_b06.py: Minimization and MD simulation of protein molecule.

## Contribution

1. Fork the repository
2. Create Feat_xxx branch
3. Commit your code
4. Create Pull Request


## Gitee Feature

1. You can use Readme\_XXX.md to support different languages, such as Readme\_en.md, Readme\_zh.md
2. Gitee blog [blog.gitee.com](https://blog.gitee.com)
3. Explore open source project [https://gitee.com/explore](https://gitee.com/explore)
4. The most valuable open source project [GVP](https://gitee.com/gvp)
5. The manual of Gitee [https://gitee.com/help](https://gitee.com/help)
6. The most popular members  [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
