# MindSPONGE

## 介绍

MindSPONGE (MindSpore Simulation Package tOwards Next Generation molecular modelling)

一款基于MindSpore开发的模块化、高通量、端到端可微的下一代智能分子模拟程序库

本程序由深圳湾实验室、华为MindSpore开发团队、北京大学、昌平实验室共同开发

开发人员：杨奕，陈迪青，张骏，夏义杰

联系方式：yangyi@szbl.ac.cn

## 软件架构

<div align=left>
<img src="docs/mindsponge.png" alt="MindSPONGE Architecture" width="600"/>
</div>

## 安装教程

本程序基于华为全场景人工智能框架MindSpore开发，使用前请先安装MindSpore：<https://mindspore.cn/>

## 使用说明

tutorials目录下为基础教程：

tutorial_b01.py: 手工创建一个模拟体系

tutorial_b02.py: 通过模板和力场参数创建一个模拟体系

tutorial_b03.py: 编辑体系与能量极小化

tutorial_b04.py: 带有偏向势的MD模拟

tutorial_b05.py: 周期性边界条件下的MD模拟

tutorial_b06.py: 蛋白质分子的能量极小化和MD模拟

## 参与贡献

1. Fork 本仓库
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request

## 特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5. Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6. Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
