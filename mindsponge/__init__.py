# Copyright 2021-2022 @ Shenzhen Bay Laboratory &
#                       Peking University &
#                       Huawei Technologies Co., Ltd
#
# This code is a part of MindSPONGE:
# MindSpore Simulation Package tOwards Next Generation molecular modelling.
#
# MindSPONGE is open-source software based on the AI-framework:
# MindSpore (https://www.mindspore.cn/)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ============================================================================
"""MindSPONGE"""

import time
from .system import Molecule, Protein
from .potential import PotentialCell, ForceFieldBase, ForceField
from .optimizer import Updater, DynamicUpdater, SteepestDescent
from .core import Sponge, SimulationCell, RunOneStepCell, AnalyseCell
from .function.units import global_units, set_global_units
from .function.units import set_global_length_unit, set_global_energy_unit


def _mindspore_version_check():
    """
       Do the MindSpore version check for MindSPONGE. If the
       MindSpore can not be imported, it will raise ImportError. If its
       version is not compatibale with current MindSponge verision,
       it will print a warning.

       Raise:
           ImportError: If the MindSpore can not be imported.
       """

    try:
        import mindspore as ms
        from mindspore import log as logger
    except ImportError:
        raise ImportError("Can not find MindSpore in current environment. Please install "
                          "MindSpore before using MindSpore Mindsponge, by following "
                          "the instruction at https://www.mindspore.cn/install")

    ms_version = ms.__version__[:5]
    required_mindspore_verision = '1.8.1'

    if ms_version < required_mindspore_verision:
        logger.warning("Current version of MindSpore is not compatible with MindSPONGE. "
                       "Some functions might not work or even raise error. Please install MindSpore "
                       "version >= {} For more details about dependency setting, please check "
                       "the instructions at MindSpore official website https://www.mindspore.cn/install "
                       "or check the README.md at https://gitee.com/mindspore/mindscience"
                       .format(required_mindspore_verision))
        warning_countdown = 3
        for i in range(warning_countdown, 0, -1):
            logger.warning(
                f"Please pay attention to the above warning, countdown: {i}")
            time.sleep(1)

_mindspore_version_check()
